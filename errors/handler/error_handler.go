package handler

import (
	"github.com/go-playground/validator/v10"

	"gitee.com/pangxianfei/frame/response"
)

type ErrorHandler interface {
	handleError(err error) *response.JsonResult
}

// HandleError 统一异常处理
func HandleError(err error) *response.JsonResult {
	switch err.(type) {
	case validator.ValidationErrors:
		return ValidationErrorHandler{}.handleError(err)
	default:
		return response.JsonFail(err.Error())
	}
}

package handler

import (
	"strings"

	"github.com/go-playground/validator/v10"

	"gitee.com/pangxianfei/frame/response"

	validatorVerifier "gitee.com/pangxianfei/frame/validator"
)

var Validator = new(validatorVerifier.Validation)

type ValidationErrorHandler struct{}

// 处理数据校验的异常信息
// 返回信息为中文提示

func (ValidationErrorHandler) handleError(err error) *response.JsonResult {
	errs := err.(validator.ValidationErrors)
	var sliceErrs []string
	for _, e := range errs {
		sliceErrs = append(sliceErrs, e.Translate(*Validator.Trans))
	}
	return response.JsonFail(strings.Join(sliceErrs, ","))
}

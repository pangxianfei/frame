package policy

import (
	"net/http"

	"gitee.com/pangxianfei/frame/library/tmaic"
	"github.com/gin-gonic/gin"
)

type UserNotPermitError struct{}

func (e UserNotPermitError) Error() string {
	return "user has no permission"
}

func Middleware(policy Policier, action Action, c Context, params []gin.Param) {

	// get route url param
	routeParamMap := make(map[string]string)
	for _, param := range params {
		routeParamMap[param.Key] = param.Value
	}

	// get user
	if err := c.ScanUser(); err != nil {
		c.JSON(http.StatusUnprocessableEntity, tmaic.V{
			"code":    http.StatusUnprocessableEntity,
			"message": err,
			"data":    nil,
		})
		c.Abort()
		return
	}

	// validate policy
	if !policyValidate(c.User(), policy, action, routeParamMap) {
		forbid(c)
		c.Abort()
		return
	}
	c.Next()
}

func forbid(c Context) {
	c.JSON(http.StatusForbidden, tmaic.V{
		"code":    http.StatusForbidden,
		"message": UserNotPermitError{}.Error(),
		"data":    nil,
	})
}

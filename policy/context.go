package policy

import (
	"gitee.com/pangxianfei/frame/context"
	"gitee.com/pangxianfei/frame/request/http/auth"
)

type Context interface {
	context.LifeCycleContextor
	context.ResponseContextor
	auth.Context
	auth.RequestIUser
}

package validator

import (
	"gitee.com/pangxianfei/frame/context"

	"gitee.com/pangxianfei/frame/resources/lang"
)

type Context interface {
	context.RequestBindingContextor
	context.ResponseContextor
	lang.Context
}

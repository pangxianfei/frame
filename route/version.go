package route

import (
	"github.com/gin-gonic/gin"

	"gitee.com/pangxianfei/frame/http/middleware"
	"gitee.com/pangxianfei/frame/request"
)

type Version struct {
	engine *request.Engine
	group  *gin.RouterGroup
	prefix string
}

func NewVersion(engine *request.Engine, prefix string) *Version {
	ver := &Version{engine: engine, prefix: prefix}
	ver.group = ver.engine.Group(prefix)
	return ver
}

func (v *Version) Auth(relativePath string, groupFunc func(grp Grouper), handlers ...request.HandlerFunc) {
	ginGroup := v.group.Group(relativePath, request.ConvertHandlers(append([]request.HandlerFunc{middleware.AuthRequired()}, handlers...))...)
	groupFunc(&group{engineHash: v.engine.Hash(), RouterGroup: ginGroup})
}

func (v *Version) NoAuth(relativePath string, groupFunc func(grp Grouper), handlers ...request.HandlerFunc) {
	ginGroup := v.group.Group(relativePath, request.ConvertHandlers(handlers)...)
	groupFunc(&group{engineHash: v.engine.Hash(), RouterGroup: ginGroup})
}

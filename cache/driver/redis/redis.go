package redis

import (
	"errors"
	"time"

	r "github.com/go-redis/redis"
	"github.com/golang/protobuf/proto"
	"github.com/jinzhu/copier"

	. "gitee.com/pangxianfei/frame/cache/utils"
	"gitee.com/pangxianfei/frame/kernel/debug"
)

func NewRedis(host string, port string, password string, dbIndex int, prefix string) *Redis {
	client := r.NewClient(&r.Options{
		Addr:     host + ":" + port,
		Password: password,
		DB:       dbIndex,
	})
	return &Redis{
		redisBasic{
			cache:  client,
			prefix: prefix,
		},
	}
}

type Redis struct {
	redisBasic
}

func (re *Redis) HSet(key string, value string, future time.Duration) bool {
	if err := re.cache.HSet(key, value, future).Err(); err != nil {
		return false
	}
	return true
}

func (re *Redis) SetNx(key string, value string, future time.Duration) bool {
	k := NewKey(key, re.Prefix())

	_, err := re.cache.Set(k.Prefixed(), value, future).Result()

	if err != nil {
		return false
	}
	return true
}

func (re *Redis) GetString(key string) string {
	k := NewKey(key, re.Prefix())

	valStr, err := re.cache.Get(k.Prefixed()).Result()
	if err != nil {
		return ""
	}
	return valStr
}
func (re *Redis) GetInt64(key string) int64 {
	k := NewKey(key, re.Prefix())
	//val := re.cache.Do("GET",key)
	debug.Dd(k)
	return re.GetInt64(key)
}

func (re *Redis) GetClient() *r.Client {
	return re.Cache()
}

func (re *Redis) Pget(key string, valuePtr proto.Message, defaultValuePtr ...proto.Message) error {
	var valueBytes []byte

	k := NewKey(key, re.Prefix())

	if !re.Has(k.Raw()) {

		if len(defaultValuePtr) > 0 {

			return copier.Copy(valuePtr, defaultValuePtr[0])
		}
		return errors.New("key not exist")
	}
	err := re.cache.Get(k.Prefixed()).Scan(&valueBytes)
	if err != nil {

		return err
	}

	if err := proto.Unmarshal(valueBytes, valuePtr); err != nil {

		return err
	}

	return nil
}

// ------------------------------------------------------------------------------
// the same
// ------------------------------------------------------------------------------

func (re *Redis) Ppull(key string, valuePtr proto.Message, defaultValuePtr ...proto.Message) error {
	k := NewKey(key, re.Prefix())

	err := re.Pget(k.Raw(), valuePtr, defaultValuePtr...)
	if err != nil {
		return err
	}

	re.Forget(k.Raw())

	return nil
}

func (re *Redis) Pput(key string, valuePtr proto.Message, future time.Duration) bool {
	valueBytes, err := proto.Marshal(valuePtr)
	if err != nil {
		return false
	}
	return re.Put(key, valueBytes, future)
}

func (re *Redis) Padd(key string, valuePtr proto.Message, future time.Duration) bool {
	valueBytes, err := proto.Marshal(valuePtr)
	if err != nil {
		return false
	}
	return re.Add(key, valueBytes, future)
}
func (re *Redis) Pforever(key string, valuePtr proto.Message) bool {
	valueBytes, err := proto.Marshal(valuePtr)
	if err != nil {
		return false
	}
	return re.Forever(key, valueBytes)
}

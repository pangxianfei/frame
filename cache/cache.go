package cache

import (
	"gitee.com/pangxianfei/frame/cache/driver"
)

type Cacher interface {
	driver.ProtoCache
	driver.BasicCache
}

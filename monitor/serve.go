package monitor

import (
	"context"
	"log"
	"net/http"
	"sync"

	"github.com/gin-gonic/gin"

	"gitee.com/pangxianfei/frame/facades"
	"gitee.com/pangxianfei/frame/kernel/zone"
	c "gitee.com/pangxianfei/frame/library/config"
	"gitee.com/pangxianfei/frame/monitor/resources/views"
	"gitee.com/pangxianfei/frame/monitor/routes"

	"gitee.com/pangxianfei/frame/request"
)

func HttpMonitorServe(parentCtx context.Context, wg *sync.WaitGroup) *http.Server {
	gin.SetMode(gin.ReleaseMode)
	r := request.New()

	routes.Register(r)

	views.Initialize(r)

	s := &http.Server{
		Addr:           ":" + c.GetString("monitor.port"),
		Handler:        r,
		ReadTimeout:    zone.Duration(c.GetInt64("app.read_timeout_seconds")) * zone.Second,
		WriteTimeout:   zone.Duration(c.GetInt64("app.write_timeout_seconds")) * zone.Second,
		MaxHeaderBytes: 1 << 20,
	}
	go func() {
		facades.Zap.Info("ws Served At Addr" + s.Addr)
		if err := s.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal(err.Error())
		}
	}()

	<-parentCtx.Done()

	facades.Zap.Info("Shutdown WS Server ...")

	ctx, cancel := context.WithTimeout(parentCtx, 5*zone.Second)
	defer cancel()

	if err := s.Shutdown(ctx); err != nil {
		facades.Zap.Fatal("Ws Server Shutdown:  error" + err.Error())
	}
	wg.Done()
	return s
}

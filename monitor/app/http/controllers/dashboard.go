package controllers

import (
	"net/http"

	controller "gitee.com/pangxianfei/frame/http/controller/gincontroller"

	"gitee.com/pangxianfei/frame/request"

	"gitee.com/pangxianfei/frame/library/config"
	"gitee.com/pangxianfei/frame/library/tmaic"
)

type Dashboard struct {
	controller.Controller
}

func (d *Dashboard) Index(c request.Context) {
	c.HTML(http.StatusOK, "dashboard.index", tmaic.V{
		"url": "ws://" + ":" + config.GetString("monitor.port") + "/monitor/dashboard/ws",
	})
	return
}

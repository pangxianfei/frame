package controllers

import (
	"errors"

	"gitee.com/pangxianfei/frame/http/controller/gincontroller"
	"gitee.com/pangxianfei/frame/monitor/app/logics/dashboard"
	"gitee.com/pangxianfei/frame/request/websocket"
)

type WebsocketController struct {
	Controller.Controller
	websocket.BaseHandler
}

func (d *WebsocketController) DefaultChannels() []string {
	return []string{"default-test-channel"}
}

func (d *WebsocketController) OnMessage(hub websocket.Hub, msg *websocket.Msg) {
	channel := &websocket.Msg{}

	//写入通道
	if msg.String() == "join test channel" {
		hub.JoinChannel("test")
	}

	//广播通道
	if msg.String() == "broadcast to test" {
		hub.BroadcastTo("test", channel)
	}

	//发送消息
	mm := &websocket.Msg{}
	if err := hub.ScanUser(); err != nil {
		mm.SetString(err.Error())
		hub.Send(mm)
		return
	}

	mm.SetJSON(hub.User().Value())
	hub.Send(mm)
	return
}

func (d *WebsocketController) Loop(hub websocket.Hub) error {

	select {
	case flow, ok := <-dashboard.Flow.Current():
		if !ok {
			return errors.New("connection closed")
		}
		msg := websocket.Msg{}
		msg.SetJSON(flow)
		hub.Broadcast(&msg)
	default:
		return nil
	}
	return nil
}

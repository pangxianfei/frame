package versions

import (
	"gitee.com/pangxianfei/frame/monitor/routes/groups"

	"gitee.com/pangxianfei/frame/request"
	"gitee.com/pangxianfei/frame/route"
)

func NewMonitor(engine *request.Engine) {
	ver := route.NewVersion(engine, "")

	ver.Auth("", func(grp route.Grouper) {
		grp.AddGroup("/im", &groups.DashboardGroup{})
	})
}

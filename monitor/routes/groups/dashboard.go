package groups

import (
	"gitee.com/pangxianfei/frame/monitor/app/http/controllers"

	"gitee.com/pangxianfei/frame/route"
)

type DashboardGroup struct {
	DashboardController          controllers.Dashboard
	DashboardWebsocketController controllers.WebsocketController
}

func (dg *DashboardGroup) Group(group route.Grouper) {
	group.GET("/", dg.DashboardController.Index)
	group.Websocket("/ws", &dg.DashboardWebsocketController)
}

package routes

import (
	"gitee.com/pangxianfei/frame/monitor/routes/versions"
	"gitee.com/pangxianfei/frame/request"
	"gitee.com/pangxianfei/frame/route"
)

func Register(router *request.Engine) {
	defer route.PrintingRouteInfo(router)

	versions.NewMonitor(router)
}

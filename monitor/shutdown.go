package monitor

import (
	"gitee.com/pangxianfei/frame/monitor/app/logics/dashboard"
)

func Shutdown() error {
	dashboard.Flow.Close()
	return nil
}

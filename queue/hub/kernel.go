package hub

import (
	"errors"
	"os"

	"gitee.com/pangxianfei/frame/console"
	"github.com/golang/protobuf/proto"

	"gitee.com/pangxianfei/frame/queue/producerconsumer"
)

func Emit(e Eventer) (errs map[ListenerName]error) {
	// For listener which has not started before emit, it will not receive the event. in SupportBroadCasting mode
	if producerconsumer.Queue().SupportBroadCasting() {
		eventListenerList := eventListener(e)
		if len(eventListenerList) <= 0 {
			errs = make(map[ListenerName]error)
			errs["nil"] = errors.New("listener doesn't exist")
			return errs
		}

		l := eventListenerList[0]
		if err := producerconsumer.NewProducer(topicName(e, l, producerconsumer.Queue().SupportBroadCasting), channelName(l), e.paramData(), l.Retries(), l.Delay()).Push(); err != nil {
			errs = make(map[ListenerName]error)
			errs[channelName(l)] = err
		}
		return errs
	}

	// push to multi Listener
	for _, l := range eventListener(e) {
		if err := producerconsumer.NewProducer(topicName(e, l, producerconsumer.Queue().SupportBroadCasting), channelName(l), e.paramData(), l.Retries(), l.Delay()).Push(); err != nil {
			if errs == nil {
				errs = make(map[ListenerName]error)
			}

			errs[channelName(l)] = err
		}
	}
	return errs
}

func On(listenerName ListenerName) {

	listener := listenerMap[listenerName]
	if listener == nil {
		listenerNameErrMsg := console.Sprintf(console.CODE_WARNING, "listener %s doesn't exist", listenerName)
		console.Println(console.CODE_WARNING, listenerNameErrMsg)
		os.Exit(1)
		return
	}

	for _, e := range listener.Subscribe() {
		err := producerconsumer.NewConsumer(topicName(e, listener, producerconsumer.Queue().SupportBroadCasting), channelName(listener), e.ParamProto(), func(paramPtr proto.Message) error {
			if err := listener.Construct(paramPtr); err != nil {
				return err
			}
			if err := listener.Handle(); err != nil {
				return err
			}
			return nil
		}).Pop()
		if err != nil {
			errMsg := console.Sprintf(console.CODE_WARNING, "listener %s doesn't exist", err.Error())
			console.Println(console.CODE_WARNING, errMsg)
			os.Exit(2)
			return
		}
	}
}

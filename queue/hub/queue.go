package hub

import (
	"gitee.com/pangxianfei/frame/queue/producerconsumer"
)

func topicName(e Eventer, l Listener, supportBroadCasting func() bool) string {
	if supportBroadCasting() {
		return "listen-" + EventName(e)
	}
	return "listen-" + EventName(e) + "-" + channelName(l)
}

func channelName(l Listener) string {
	return l.Name()
}
func RegisterQueue() {
	for e, llist := range hub {
		for _, l := range llist {
			if err := producerconsumer.Queue().Register(topicName(event(e), l, producerconsumer.Queue().SupportBroadCasting), channelName(l)); err != nil {
				panic(err)
			}
		}
	}
}

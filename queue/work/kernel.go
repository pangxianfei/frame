package work

import (
	"os"

	"gitee.com/pangxianfei/frame/library/console"

	"gitee.com/pangxianfei/frame/queue/producerconsumer"
)

func Dispatch(j worker) error {

	if err := producerconsumer.NewProducer(topicName(j), channelName(j), j.paramData(), j.Retries(), j.Delay()).Push(); err != nil {
		return err
	}
	return nil
}

func Process(jobName string) {
	j := jobMap[jobName]
	if j == nil {
		jobNameErrMsg := console.Sprintf(console.CODE_WARNING, "job %s doesn't exist", jobName)
		console.Println(console.CODE_WARNING, jobNameErrMsg)
		os.Exit(1)
		return
	}
	err := producerconsumer.NewConsumer(topicName(j), channelName(j), j.ParamProto(), j.Handle).Pop()
	if err != nil {
		errMsg := console.Sprintf(console.CODE_WARNING, "worker %s doesn't exist", err.Error())
		console.Println(console.CODE_WARNING, errMsg)
		os.Exit(1)
		return
	}
}

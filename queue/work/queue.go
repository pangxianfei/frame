package work

import (
	"os"

	"gitee.com/pangxianfei/frame/library/console"

	"gitee.com/pangxianfei/frame/queue/producerconsumer"
)

func topicName(j worker) string {
	return "work-" + j.Name()
}
func channelName(j worker) string {
	return j.Name()
}
func RegisterQueue() {
	for _, j := range jobMap {
		if err := producerconsumer.Queue().Register(topicName(j), channelName(j)); err != nil {
			console.Println(console.CODE_WARNING, err.Error())
			os.Exit(1)
		}
	}
}

package producerconsumer

import (
	"gitee.com/pangxianfei/frame/library/config"

	"gitee.com/pangxianfei/frame/kernel/zone"
	"gitee.com/pangxianfei/frame/queue/driver/nsq"
)

func Initialize() {
	//@todo use different config to new different queuer
	//@todo memory, nsq, rabbitmq
	setQueue(nsq.NewNsq(config.GetString("queue.default")))
	initializeFailedProcessor()
}

var queue Queuer

func Queue() Queuer {
	return queue
}
func setQueue(q Queuer) {
	queue = q
}

type Queuer interface {
	producerer
	consumerer
	registerer
	SupportBroadCasting() bool
	Close() (err error)
}

type registerer interface {
	Register(topicName string, channelName string) (err error)
	Unregister(topicName string, channelName string) (err error)
}

type producerer interface {
	Push(topicName string, channelName string, delay zone.Duration, body []byte) (err error)
}
type consumerer interface {
	Pop(topicName string, channelName string, handler func(hash string, body []byte) (handlerErr error), maxInFlight int) (err error)
}

package model

type Modeller interface {
	TableName() string
	Default() interface{}
}

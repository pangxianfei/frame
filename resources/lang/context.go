package lang

import "gitee.com/pangxianfei/frame/context"

type Context interface {
	context.DataContextor
}

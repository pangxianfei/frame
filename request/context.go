package request

import (
	"github.com/gin-gonic/gin"

	"gitee.com/pangxianfei/frame/context"
	"gitee.com/pangxianfei/frame/request/http/auth"
	"gitee.com/pangxianfei/frame/request/http/tenantdb"
	"gitee.com/pangxianfei/frame/utils/jwt"
)

type Context interface {
	context.HttpContextor
	GinContext() *gin.Context
	SetAuthClaim(claims *jwt.UserClaims)
	SetIUserModel(iUser auth.IUser)
	auth.Context
	auth.RequestIUser
	tenantdb.DbTenantManager
}

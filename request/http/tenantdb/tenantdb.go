package tenantdb

import (
	"gorm.io/gorm"
)

type DbTenantManager interface {
	Db() *gorm.DB
	AppDb(AppId int64) *gorm.DB
	TenantDb(tenantId int64, AppId int64) *gorm.DB
	ContextTenantDb() *gorm.DB
}

package websocket

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"

	"gitee.com/pangxianfei/frame/facades"
	"gitee.com/pangxianfei/frame/kernel/zone"
	"gitee.com/pangxianfei/frame/library/tmaic"
	requesthttp "gitee.com/pangxianfei/frame/request/http"
)

func ConvertHandler(wsHandler Handler) gin.HandlerFunc {
	var wsUpgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
	var pingPeriod = (wsHandler.ReadTimeout() * 9) / 10
	return func(c *gin.Context) {
		totovalContext := requesthttp.ConvertContext(c)

		// create connectionHub
		hub := newConnectionHub(totovalContext, wsHandler)

		ws, err := wsUpgrader.Upgrade(totovalContext.Writer(), totovalContext.Request(), nil)
		if err != nil {
			facades.Zap.Error("msg Failed to set websocket upgrade :" + err.Error())
			totovalContext.JSON(http.StatusUnprocessableEntity, tmaic.V{"error": err})
			return
		}
		// close ws connection
		defer func() {
			if err := ws.Close(); err != nil {
				err.Error()
			}
			hub.close()
		}()

		// handle panic
		defer func() {
			if _err := recover(); _err != nil {
				if __err, ok := _err.(error); ok {
					err = __err
					return
				}
				err = errors.New(fmt.Sprint(_err))
				return
			}
		}()

		// setMaxSize
		ws.SetReadLimit(wsHandler.MaxMessageSize())

		// set handlers
		setPingPongCloseHandlers(ws, wsHandler, hub)

		// --------------------------
		// ---- websocket process ----
		// --------------------------

		// send msg
		go func() {
			pingTicker := zone.NewTicker(pingPeriod)
			defer pingTicker.Stop()

			for {
				if err := wsHandler.Loop(hub); err != nil {
					return
				}

				select {
				// send message
				case msg, ok := <-hub.getChan():
					if !ok {
						return
					}
					if msg.isDone() {
						return
					}
					if err := msg.send(ws, wsHandler); err != nil {
						err.Error()
						return
					}

				// send ping
				case <-pingTicker.C:
					ping := Msg{
						msgType: websocket.PingMessage,
						data:    &[]byte{},
						err:     nil,
					}
					if err := ping.send(ws, wsHandler); err != nil {
						err.Error()
						return
					}

				default:
					continue
				}
			}
		}()

		// read msg
		for {
			msg := &Msg{}
			if msg.scan(ws, wsHandler) != nil {
				err.Error()
				return
			}

			switch msg.msgType {
			// TextMessage denotes a text data message. The text message payload is
			// interpreted as UTF-8 encoded text data.
			case websocket.TextMessage:
				fallthrough
			// BinaryMessage denotes a binary data message.
			case websocket.BinaryMessage:
				wsHandler.OnMessage(hub, msg)
			// PingMessage denotes a ping control message. The optional message payload
			// is UTF-8 encoded text.
			case websocket.PingMessage: // defined at SetPingHandler
				// send pong
				pong := Msg{
					msgType: websocket.PongMessage,
					data:    &[]byte{},
					err:     nil,
				}
				if err := pong.send(ws, wsHandler); err != nil {
					err.Error()
					return
				}
			// PongMessage denotes a pong control message. The optional message payload
			// is UTF-8 encoded text.
			case websocket.PongMessage: // defined at SetPongHandler
				// check deadline
				if msg.err != nil {
					err.Error()
					return
				}
			case websocket.CloseMessage: // defined at SetCloseHandler
				closeMsg := Msg{
					msgType: websocket.CloseMessage,
					data:    &[]byte{},
					err:     nil,
				}
				if err := closeMsg.send(ws, wsHandler); err != nil {
					err.Error()
					return
				}

				return
			default:
				facades.Zap.Warn("No websocket handler on this msgType msgType :" + fmt.Sprintf("%d", msg.msgType))
			}

		}
	}
}

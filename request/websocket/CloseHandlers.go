package websocket

import (
	"github.com/gorilla/websocket"
)

func PingPongCloseHandlers(ws *websocket.Conn, wsHandler Handler, hub Hub) {

	ws.SetCloseHandler(func(code int, text string) error {
		wsHandler.OnClose(hub, code, text)
		return nil
	})
	ws.SetPingHandler(func(appData string) error {
		wsHandler.OnPing(hub, appData)
		return nil
	})
	ws.SetPongHandler(func(appData string) error {
		wsHandler.OnPing(hub, appData)
		return nil
	})
}
func setPingPongCloseHandlers(ws *websocket.Conn, wsHandler Handler, hub Hub) {

	ws.SetCloseHandler(func(code int, text string) error {
		wsHandler.OnClose(hub, code, text)
		return nil
	})
	ws.SetPingHandler(func(appData string) error {
		wsHandler.OnPing(hub, appData)
		return nil
	})
	ws.SetPongHandler(func(appData string) error {
		wsHandler.OnPing(hub, appData)
		return nil
	})
}

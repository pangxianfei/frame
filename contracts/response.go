package response

import (
	"gitee.com/pangxianfei/frame/response"
	"github.com/kataras/iris/v12"
)

type Response interface {
	JSON(ctx iris.Context, data interface{}) *response.JsonResult
}

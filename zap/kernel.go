package zap

import "gitee.com/pangxianfei/frame/facades"

func Initialize() {
	facades.Zap = NewZapApplication()
}

package zap

import (
	"os"

	"gitee.com/pangxianfei/frame/library/config"

	zapLog "go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"

	"gitee.com/pangxianfei/frame/facades"
)

type Zap struct {
	Logger *zapLog.Logger
}

func NewZapApplication() *zapLog.Logger {
	hook := lumberjack.Logger{
		Filename:   config.GetString("zap.filename"), // 日志文件路径
		MaxSize:    config.GetInt("zap.max_size"),    // 每个日志文件保存的最大尺寸 单位：M
		MaxBackups: config.GetInt("zap.max_backup"),  // 日志文件最多保存多少个备份
		MaxAge:     config.GetInt("zap.max_age"),     // 文件最多保存多少天
		Compress:   config.GetBool("zap.compress"),   // 是否压缩
	}

	encoderConfig := zapcore.EncoderConfig{
		TimeKey:        "time",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "linenum",
		MessageKey:     "msg",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.CapitalLevelEncoder,    // 编码器
		EncodeTime:     zapcore.ISO8601TimeEncoder,     // ISO8601 UTC 时间格式
		EncodeDuration: zapcore.SecondsDurationEncoder, //
		EncodeCaller:   zapcore.ShortCallerEncoder,     // 路径编码器
		EncodeName:     zapcore.FullNameEncoder,
	}

	// 设置日志级别
	atomicLevel := zapLog.NewAtomicLevel()
	atomicLevel.SetLevel(zapLog.InfoLevel)

	core := zapcore.NewCore(
		zapcore.NewConsoleEncoder(encoderConfig),                                        // 编码器配置
		zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout), zapcore.AddSync(&hook)), // 打印到控制台和文件
		atomicLevel, // 日志级别
	)

	// 开启开发模式，堆栈跟踪
	caller := zapLog.AddCaller()
	// 开启文件及行号
	development := zapLog.Development()

	// 构造日志
	Logger := zapLog.New(core, caller, development)
	facades.Logger = Logger
	facades.Zap = Logger
	return Logger
}

/*
func (z *Zap) Debug(args ...interface{}) {
	//TODO implement me
	panic("implement me")
}

func (z *Zap) Debugf(format string, args ...interface{}) {
	//TODO implement me
	panic("implement me")
}

func (z *Zap) Info(args ...interface{}) {
	//TODO implement me
	panic("implement me")
}

func (z *Zap) Infof(format string, args ...interface{}) {
	//TODO implement me
	panic("implement me")
}

func (z *Zap) Warning(args ...interface{}) {
	//TODO implement me
	panic("implement me")
}

func (z *Zap) Warningf(format string, args ...interface{}) {
	//TODO implement me
	panic("implement me")
}

func (z *Zap) Error(args ...interface{}) {
	//TODO implement me
	panic("implement me")
}

func (z *Zap) Errorf(format string, args ...interface{}) {
	//TODO implement me
	panic("implement me")
}

func (z *Zap) Fatal(args ...interface{}) {
	//TODO implement me
	panic("implement me")
}

func (z *Zap) Fatalf(format string, args ...interface{}) {
	//TODO implement me
	panic("implement me")
}

func (z *Zap) Panic(args ...interface{}) {
	//TODO implement me
	panic("implement me")
}

func (z *Zap) Panicf(format string, args ...interface{}) {
	//TODO implement me
	panic("implement me")
}
*/

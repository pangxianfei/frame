package app

import (
	"net/http"
	"sync"

	"gitee.com/pangxianfei/frame/library/config"
	"gitee.com/pangxianfei/frame/library/tmaic"
	"github.com/gin-gonic/gin"

	"gitee.com/pangxianfei/frame/facades"
	"gitee.com/pangxianfei/frame/http/middleware"
	"gitee.com/pangxianfei/frame/kernel/zone"
	"gitee.com/pangxianfei/frame/request"
)

func CreateHttpServer(portServer string, wg *sync.WaitGroup) *http.Server {
	gin.SetMode(gin.DebugMode)
	r := request.New()
	r.Use(middleware.RequestLogger())
	r.Use(middleware.Logger())
	r.Use(middleware.Recovery())
	r.Use(middleware.Locale())

	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"code": http.StatusOK, "Message": "Welcome Serve" + portServer})
	})
	server := &http.Server{
		Addr:           ":" + portServer,
		Handler:        r,
		ReadTimeout:    zone.Duration(config.GetInt64("app.read_timeout_seconds")) * zone.Second,
		WriteTimeout:   zone.Duration(config.GetInt64("app.write_timeout_seconds")) * zone.Second,
		MaxHeaderBytes: 1 << 20,
	}
	facades.Log.Info("Served At", tmaic.V{"Addr": server.Addr})
	return server
}

package cmd

import (
	"fmt"
	"os"
	"strings"

	"github.com/iancoleman/strcase"

	"gitee.com/pangxianfei/frame/support/file"
	"gitee.com/pangxianfei/frame/support/str"
)

type Models struct {
	AppName            string
	TableName          string
	StructName         string
	StructNamePlural   string
	VariableName       string
	VariableNamePlural string
	PackageName        string
	ModelFileName      string
	ServiceName        string
	RepositoriesName   string
}

type Model struct {
}

// MakeModelFromString 格式化用户输入的内容
func (s *Model) MakeModelFromString(appname string, name string) Models {
	model := Models{}
	model.AppName = appname
	model.StructName = str.Singular(strcase.ToCamel(name))
	model.StructNamePlural = str.Plural(model.StructName)
	model.TableName = str.Snake(model.StructNamePlural)
	model.VariableName = str.LowerCamel(model.StructName)
	model.PackageName = str.Snake(model.StructName)
	model.VariableNamePlural = str.LowerCamel(model.StructNamePlural)
	model.ModelFileName = str.Camel(model.StructName)
	model.ServiceName = str.Camel(model.StructName)
	model.RepositoriesName = str.Camel(model.StructName)

	return model
}

// CreateFileFromStub 读取 stub 文件并进行变量替换
// 最后一个选项可选，如若传参，应传 map[string]string 类型，作为附加的变量搜索替换
func (s *Model) CreateFileFromStub(filePath string, stubName string, model Models, variables ...interface{}) {

	// 实现最后一个参数可选
	replaces := make(map[string]string)
	if len(variables) > 0 {
		replaces = variables[0].(map[string]string)
	}
	//debug.Dd(stubName + ".stub")
	// 目标文件已存在
	if file.Exists(filePath) {
		Success(fmt.Sprintf("=> %s already exists!", filePath))
	} else {

		// 读取 stub 模板文件
		modelData, err := os.ReadFile(stubName + ".stub")

		if err != nil {
			Exit("读取 stub 模板文件失败 - 1")
		}
		modelStub := string(modelData)
		// 添加默认的替换变量
		replaces["{{VariablAppName}}"] = model.AppName
		replaces["{{VariableName}}"] = model.VariableName
		replaces["{{VariableNamePlural}}"] = model.VariableNamePlural
		replaces["{{StructName}}"] = model.StructName
		replaces["{{StructNamePlural}}"] = model.StructNamePlural
		replaces["{{PackageName}}"] = model.PackageName
		replaces["{{TableName}}"] = model.TableName
		// 对模板内容做变量替换
		for search, replace := range replaces {
			modelStub = strings.ReplaceAll(modelStub, search, replace)
		}
		// 存储到目标文件中
		err = file.Put([]byte(modelStub), filePath)
		if err != nil {
			Exit("写入文件失败")
		}
		// 提示成功
		Success(fmt.Sprintf("=> %s created", filePath))
	}

}

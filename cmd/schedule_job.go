package cmd

import (
	"errors"

	"gitee.com/pangxianfei/frame/facades"
)

type Job struct {
	scmd *ScheduleCommand
}

func NewJob(scmd *ScheduleCommand) *Job {
	return &Job{scmd: scmd}
}

func (j *Job) Run() {
	defer func(command *ScheduleCommand) {
		if err := recover(); err != nil {
			CommandErr := errors.New(command.When() + "schedule panic")
			if checkErr, ok := err.(error); ok {
				CommandErr = checkErr
			}
			facades.Log.Error(CommandErr)
		}
	}(j.scmd)

	if err := j.scmd.handler()(j.scmd.args()); err != nil {
		facades.Log.Error(err)
	}
}

package cmd

import (
	"fmt"

	"gitee.com/pangxianfei/frame/cmd/schedule"
)

type ScheduleCommand struct {
	commandHandler func(arg *Arg) error
	arg            *Arg
	when           schedule.When
}

func newScheduleCommand(handler func(arg *Arg) error, arg *Arg) *ScheduleCommand {
	return &ScheduleCommand{commandHandler: handler, arg: arg}
}

func (sc *ScheduleCommand) Yearly() {
	sc.when = schedule.EveryYear
}
func (sc *ScheduleCommand) Monthly() {
	sc.when = schedule.EveryMonth
}
func (sc *ScheduleCommand) Daily() {
	sc.when = schedule.EveryDay
}
func (sc *ScheduleCommand) Hourly() {
	sc.when = schedule.EveryHour
}
func (sc *ScheduleCommand) EveryMinute() {
	sc.when = schedule.EveryMinute
}
func (sc *ScheduleCommand) EverySecond() {
	sc.when = schedule.EverySecond
}
func (sc *ScheduleCommand) EveryDays(d uint) {
	sc.when = fmt.Sprintf(schedule.EveryHoursFormat, d*24)
}
func (sc *ScheduleCommand) EveryHours(h uint) {
	sc.when = fmt.Sprintf(schedule.EveryHoursFormat, h)
}
func (sc *ScheduleCommand) EveryMinutes(m uint) {
	sc.when = fmt.Sprintf(schedule.EveryMinutesFormat, m)
}
func (sc *ScheduleCommand) EverySeconds(s uint) {
	sc.when = fmt.Sprintf(schedule.EverySecondsFormat, s)
}

func (sc *ScheduleCommand) When() schedule.When {
	return sc.when
}
func (sc *ScheduleCommand) args() *Arg {
	return sc.arg
}
func (sc *ScheduleCommand) handler() func(arg *Arg) error {
	return sc.commandHandler
}

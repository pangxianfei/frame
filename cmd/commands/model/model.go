package model

import (
	"fmt"
	"os"

	"gitee.com/pangxianfei/frame/library/config"

	"gitee.com/pangxianfei/frame/cmd"
)

func init() {
	cmd.Add(&Model{})
}

type Model struct {
}

func (s *Model) Command() string {
	return "model:make {appname} {modelname}"
}

func (s *Model) Description() string {
	return "Create a Model"
}

func (s *Model) Handler(arg *cmd.Arg) error {
	appname, err := arg.Get("appname")
	modelname, err := arg.Get("modelname")

	if err != nil {
		return err
	}

	if appname == nil {
		fmt.Println("You need a App name")
		return nil
	}

	if modelname == nil {
		fmt.Println("You need a Model name")
		return nil
	}

	var AppPath string = config.GetString("app.app_path")
	var CreateModelPath string = "%s/%s/models/%s/"
	var stubsModelPath string = fmt.Sprintf("%s/make/stubs/model/", config.GetString("app.make_stubs"))

	newAppName := string(*appname)
	newName := string(*modelname)
	createModel := cmd.Model{}

	Models := createModel.MakeModelFromString(newAppName, newName)

	dir := fmt.Sprintf(CreateModelPath, AppPath, newAppName, Models.PackageName)
	_ = os.MkdirAll(dir, os.ModePerm)

	// 替换变量
	createModel.CreateFileFromStub(dir+Models.ModelFileName+"Model.go", stubsModelPath+"/model", Models)
	createModel.CreateFileFromStub(dir+Models.ModelFileName+"Util.go", stubsModelPath+"/model_util", Models)
	createModel.CreateFileFromStub(dir+Models.ModelFileName+"Hooks.go", stubsModelPath+"/model_hooks", Models)

	return nil
}

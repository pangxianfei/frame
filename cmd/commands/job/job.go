package job

import (
	"fmt"

	"gitee.com/pangxianfei/frame/library/config"

	"gitee.com/pangxianfei/frame/cmd"
	"gitee.com/pangxianfei/frame/kernel/debug"
)

func init() {
	cmd.Add(&job{})
}

type job struct {
}

func (s *job) Command() string {
	return "job:make {appname} {name}"
}

func (s *job) Description() string {
	return "Create a AppName Job"
}

func (s *job) Handler(arg *cmd.Arg) error {
	appname, aErr := arg.Get("appname")
	if aErr != nil {
		return aErr
	}
	modelName, rErr := arg.Get("name")
	if rErr != nil {
		return rErr
	}

	if appname == nil {
		cmd.Warning("You need a app name")
		return nil
	}

	if modelName == nil {
		cmd.Warning("You need a services name")
		return nil
	}

	var AppPath string = config.GetString("app.app_path")

	var stubsJobnametPath string = fmt.Sprintf("%s/make/stubs/job", config.GetString("app.make_stubs"))
	var stubsJobPbnametPath string = fmt.Sprintf("%s/make/stubs/job.pb", config.GetString("app.make_stubs"))
	debug.Dd(stubsJobnametPath)
	debug.Dd(stubsJobPbnametPath)
	newAppName := string(*appname)
	newRequestName := string(*modelName)

	createModel := cmd.Model{}
	Models := createModel.MakeModelFromString(newAppName, newRequestName)
	// 拼接目标文件路径
	filePath := fmt.Sprintf("%s/%s/jobs/%sJob.go", AppPath, Models.AppName, Models.StructName)
	debug.Dd(filePath)
	// 基于模板创建文件（做好变量替换）
	createModel.CreateFileFromStub(filePath, stubsJobnametPath, Models)

	// 拼接目标文件路径
	filePath = fmt.Sprintf("%s/%s/jobs/proto3/%sJobs.proto", AppPath, Models.AppName, Models.StructName)
	// 基于模板创建文件（做好变量替换）
	createModel.CreateFileFromStub(filePath, stubsJobPbnametPath, Models)

	return nil
}

package controller

import (
	"fmt"
	"os"
	"strings"

	"gitee.com/pangxianfei/frame/library/config"

	"gitee.com/pangxianfei/frame/cmd"
)

func init() {
	cmd.Add(&Controller{})
}

type Controller struct {
}

func (s *Controller) Command() string {
	return "controller:make {appname} {name}"
}

func (s *Controller) Description() string {
	return "Create api controller，exmaple: make controller v1/user"
}

func (s *Controller) Handler(arg *cmd.Arg) error {
	appname, err := arg.Get("appname")
	name, err := arg.Get("name")
	if err != nil {
		cmd.Exit(err.Error())
		return err
	}

	// 处理参数，要求附带 API 版本（v1 或者 v2）
	array := strings.Split(*name, "/")
	if len(array) != 2 {
		cmd.Exit("api controller name format: v1/user")
	}

	newAppName := string(*appname)
	newName := string(array[1])

	var AppPath string = config.GetString("app.app_path")

	var CreateModelPath string = "%s/%s/models/%s/"
	var stubsModelPath string = fmt.Sprintf("%s/make/stubs/model/", config.GetString("app.make_stubs"))
	var stubsControllerPath string = fmt.Sprintf("%s/make/stubs/controller", config.GetString("app.make_stubs"))

	createModel := cmd.Model{}
	Models := createModel.MakeModelFromString(newAppName, newName)
	dir := fmt.Sprintf(CreateModelPath, AppPath, newAppName, Models.PackageName)
	_ = os.MkdirAll(dir, os.ModePerm)
	// 替换变量
	createModel.CreateFileFromStub(dir+Models.ModelFileName+"Model.go", stubsModelPath+"/model", Models)
	createModel.CreateFileFromStub(dir+Models.ModelFileName+"Util.go", stubsModelPath+"/model_util", Models)
	createModel.CreateFileFromStub(dir+Models.ModelFileName+"Hooks.go", stubsModelPath+"/model_hooks", Models)

	// 控制器版本号
	apiVersion := array[0]
	// 组建目标目录
	filePath := fmt.Sprintf("%s/%s/http/controllers/%s/%sController.go", AppPath, newAppName, apiVersion, Models.StructName)
	// 基于模板创建文件（做好变量替换）
	createModel.CreateFileFromStub(filePath, stubsControllerPath, Models)
	return nil
}

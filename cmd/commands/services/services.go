package services

import (
	"fmt"

	"gitee.com/pangxianfei/frame/library/config"

	"gitee.com/pangxianfei/frame/cmd"
)

func init() {
	cmd.Add(&Services{})
}

type Services struct {
}

func (s *Services) Command() string {
	return "services:make {appname} {name}"
}

func (s *Services) Description() string {
	return "Create a AppName services"
}

func (s *Services) Handler(arg *cmd.Arg) error {
	appname, aErr := arg.Get("appname")
	if aErr != nil {
		return aErr
	}
	servicesname, rErr := arg.Get("name")
	if rErr != nil {
		return rErr
	}

	if appname == nil {
		cmd.Warning("You need a app name")
		return nil
	}

	if servicesname == nil {
		cmd.Warning("You need a services name")
		return nil
	}
	var AppPath string = config.GetString("app.app_path")
	var stubsServicesNamePath string = fmt.Sprintf("%s/make/stubs/services", config.GetString("app.make_stubs"))
	var stubsRepositoryNamePath string = fmt.Sprintf("%s/make/stubs/repository", config.GetString("app.make_stubs"))

	newAppName := string(*appname)
	newRequestName := string(*servicesname)

	createModel := cmd.Model{}
	Models := createModel.MakeModelFromString(newAppName, newRequestName)
	// 拼接目标文件路径
	filePath := fmt.Sprintf("%s/%s/services/%sServices.go", AppPath, Models.AppName, Models.StructName)

	// 基于模板创建文件（做好变量替换）
	createModel.CreateFileFromStub(filePath, stubsServicesNamePath, Models)

	// 拼接目标文件路径
	filePath = fmt.Sprintf("%s/%s/repositories/%sRepository.go", AppPath, Models.AppName, Models.StructName)
	// 基于模板创建文件（做好变量替换）
	createModel.CreateFileFromStub(filePath, stubsRepositoryNamePath, Models)

	return nil
}

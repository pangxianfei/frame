package mq

import (
	"fmt"

	"gitee.com/pangxianfei/frame/library/config"

	"gitee.com/pangxianfei/frame/cmd"
)

func init() {
	cmd.Add(&mq{})
}

type mq struct {
}

func (s *mq) Command() string {
	return "mq:make {appname} {name}"
}

func (s *mq) Description() string {
	return "Create a AppName mq Object"
}

func (s *mq) Handler(arg *cmd.Arg) error {
	appname, aErr := arg.Get("appname")
	if aErr != nil {
		return aErr
	}
	mqname, rErr := arg.Get("name")
	if rErr != nil {
		return rErr
	}

	if appname == nil {
		cmd.Warning("You need a app name")
		return nil
	}

	if mqname == nil {
		cmd.Warning("You need a mq name")
		return nil
	}

	var AppPath string = config.GetString("app.app_path")
	var stubsMqMamePath string = fmt.Sprintf("%s/make/stubs/mq", config.GetString("app.make_stubs"))

	newAppName := string(*appname)
	newMqName := string(*mqname)

	createModel := cmd.Model{}
	Models := createModel.MakeModelFromString(newAppName, newMqName)
	// 拼接目标文件路径
	filePath := fmt.Sprintf("%s/%s/mq/%s.go", AppPath, Models.AppName, Models.StructName)

	// 基于模板创建文件（做好变量替换）
	createModel.CreateFileFromStub(filePath, stubsMqMamePath, Models)

	return nil
}

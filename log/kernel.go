package log

import "gitee.com/pangxianfei/frame/facades"

func Initialize() {
	facades.Log = NewLogrusApplication()
}

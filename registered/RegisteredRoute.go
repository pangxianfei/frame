package Registered

import (
	"bytes"
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"

	"gitee.com/pangxianfei/frame/response"

	"gitee.com/pangxianfei/frame/errors/handler"
	"gitee.com/pangxianfei/frame/facades"
	"gitee.com/pangxianfei/frame/request"
)

const (
	ServerInnerError = "服务器内部错误，请联系管理员处理"
)

type BodyDumpResponseWriter struct {
	gin.ResponseWriter
	body *bytes.Buffer
}

func (w *BodyDumpResponseWriter) Write(b []byte) (int, error) {
	w.body.Write(b) // 注意这一行
	return w.ResponseWriter.Write(b)
}

type RouteFunc func(ctx request.Context) *response.JsonResult

// defer处理panic
func handlePanic(ctx request.Context) {
	if err := recover(); err != nil {
		r := response.Fail()
		switch err.(type) {
		case error:
			facades.Logger.Error(err.(error).Error())
			r = handler.HandleError(err.(error))
		case string:
			facades.Logger.Error(err.(string))
			r = handler.HandleError(errors.New(err.(string)))
		default:
			facades.Logger.Error(ServerInnerError)
			r = response.JsonFail(ServerInnerError)
		}
		response.JSON(ctx, r)
	}
}

func Route(wrapper RouteFunc) request.HandlerFunc {
	// 日志处理
	return func(ctx request.Context) {
		//拦截上一层响应状态，如果状态不等200返回
		if ctx.Writer().Status() != http.StatusOK {
			ctx.Abort()
			return
		}
		defer handlePanic(ctx)
		resp := wrapper(ctx)
		if resp.Err != nil {
			facades.Logger.Error(resp.Err.Error())
			resp = handler.HandleError(resp.Err)
		}
		response.JSON(ctx, resp)
	}
}

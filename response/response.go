package response

import (
	"net/http"

	"gitee.com/pangxianfei/frame/library/tmaic"

	"gitee.com/pangxianfei/frame/request"
)

const (
	FAIL = 1
	OK   = 0
)

func Fail() *JsonResult {
	return Json(FAIL, "操作失败", map[string]interface{}{}, false)
}

func Json(code int, message string, data interface{}, success bool) *JsonResult {
	return &JsonResult{
		Code:    code,
		Message: message,
		Data:    data,
		Success: success,
	}
}

func JsonData(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Data:    data,
		Success: true,
	}
}

// JsonCreateSuccess 创建成功返回专用
func JsonCreateSuccess(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Message: "创建成功",
		Data:    data,
		Success: true,
	}
}

// JsonCreateFail 创建失败返回专用
func JsonCreateFail(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    201,
		Message: "创建失败,请稍后尝试~",
		Data:    data,
		Success: false,
	}
}

// JsonQueryData 查询返回专用
func JsonQueryData(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Message: "查询成功",
		Data:    data,
		Success: true,
	}
}

// JsonUpdateSuccess 更新成功返回专用
func JsonUpdateSuccess(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Message: "更新成功",
		Data:    data,
		Success: true,
	}
}

func JsonUpdateFail(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Message: "更新失败,请稍后尝试~",
		Data:    data,
		Success: true,
	}
}

func JsonDeleteSuccess(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Message: "删除成功",
		Data:    data,
		Success: true,
	}
}

func JsonDeleteFail(data string) *JsonResult {
	return &JsonResult{
		Code:    201,
		Message: "删除失败,请稍后尝试~",
		Data:    data,
		Success: true,
	}
}

func JsonItemList(data []interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Data:    data,
		Success: true,
	}
}

func JsonSuccess() *JsonResult {
	return &JsonResult{
		Code:    200,
		Message: "请求成功",
		Data:    nil,
		Success: true,
	}
}

func JsonErrorMsg(message string) *JsonResult {
	return &JsonResult{
		Code:    201,
		Message: message,
		Data:    nil,
		Success: false,
	}
}

func JsonError(err error) *JsonResult {
	return &JsonResult{
		Code:    201,
		Message: err.Error(),
		Data:    nil,
		Success: false,
	}
}

// JsonFail 请求失败专用
func JsonFail(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    201,
		Message: "请求失败,请稍后尝试~",
		Data:    data,
		Success: false,
	}
}

func JsonErrorCode(code int, message string) *JsonResult {
	return &JsonResult{
		Code:    code,
		Message: message,
		Data:    nil,
		Success: false,
	}
}

func JsonErrorData(code int, message string, data interface{}) *JsonResult {
	return &JsonResult{
		Code:    code,
		Message: message,
		Data:    data,
		Success: false,
	}
}

func JsonDataError(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    401,
		Message: "提交失败,请稍后尝试~",
		Data:    data,
		Success: false,
	}
}

// JSON gin 框架使用
func JSON(ctx request.Context, resp *JsonResult) {
	// 响应客户端
	ctx.JSON(http.StatusOK, tmaic.V{
		"code":    resp.Code,
		"Message": resp.Message,
		"data":    resp.Data,
		"Success": 200 == http.StatusOK,
	})
}

// Unauthorized JSON gin 框架使用
func Unauthorized(ctx request.Context) {
	// 响应客户端
	ctx.JSON(http.StatusUnauthorized, tmaic.V{
		"code":    http.StatusUnauthorized,
		"Message": "未认证登录",
		"data":    "未认证登录",
		"Success": false,
	})
}

type RspBuilder struct {
	Data map[string]interface{}
}

func (builder *RspBuilder) Put(key string, value interface{}) *RspBuilder {
	builder.Data[key] = value
	return builder
}

func (builder *RspBuilder) Build() map[string]interface{} {
	return builder.Data
}

func (builder *RspBuilder) JsonResult() *JsonResult {
	return JsonData(builder.Data)
}

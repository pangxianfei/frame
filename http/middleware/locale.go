package middleware

import (
	"gitee.com/pangxianfei/frame/library/config"

	l "gitee.com/pangxianfei/frame/kernel/locale"

	"gitee.com/pangxianfei/frame/request"
)

func Locale() request.HandlerFunc {
	return func(c request.Context) {
		locale := c.Request().Header.Get("locale")
		if locale == "" {
			locale = c.DefaultQuery("locale", config.GetString("app.locale"))
		}

		l.SetLocale(c, locale)

		c.Next()
	}
}

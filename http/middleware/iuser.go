package middleware

import (
	"gitee.com/pangxianfei/frame/request"
	"gitee.com/pangxianfei/frame/request/http/auth"
)

func IUser(userModelPtr auth.IUser) request.HandlerFunc {
	return func(c request.Context) {
		c.SetIUserModel(userModelPtr)

		c.Next()
	}
}

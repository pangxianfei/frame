package middleware

import (
	"net/http"
	"strings"

	"gitee.com/pangxianfei/frame/library/tmaic"

	"gitee.com/pangxianfei/frame/request"

	"gitee.com/pangxianfei/frame/utils/jwt"
)

const (
	ContextTokenKey = "TMAIC_CONTEXT_TOKEN"
)

type TokenRevokeError struct{}

func (e TokenRevokeError) Error() string {
	return "token revoke failed"
}

func AuthRequired() request.HandlerFunc {
	return func(c request.Context) {
		token := c.DefaultQuery("token", "")
		if token == "" {
			token = c.Request().Header.Get("Authorization")
			if s := strings.Split(token, " "); len(s) == 2 {
				token = s[1]
			}
		}
		// set token
		c.Set(ContextTokenKey, token)
		j := jwt.NewJWT()
		claims, err := j.ParseToken(token)
		if err != nil {
			if err == jwt.TokenExpired {
				if token, _err := j.RefreshTokenUnverified(token); _err == nil {
					if claims, err := j.ParseToken(token); err == nil {
						c.SetAuthClaim(claims)
						c.Header("Authorization", "Bear "+token)
						return
					}
				}
			}
			c.AbortWithStatusJSON(http.StatusUnauthorized, tmaic.V{"Message": err.Error()})
			return
		}
		c.SetAuthClaim(claims)
	}
}

func Revoke(c request.Context) error {
	j := jwt.NewJWT()
	if tokenString, exist := c.Get(ContextTokenKey); exist {
		if token, ok := tokenString.(string); ok {
			if err := j.RevokeToken(token); err == nil {
				c.Header("Authorization", "")
				return nil
			}
		}
	}
	return TokenRevokeError{}
}

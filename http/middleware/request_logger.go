package middleware

import (
	"bytes"
	"fmt"
	"io/ioutil"

	"gitee.com/pangxianfei/frame/library/config"
	"github.com/gin-gonic/gin"

	"gitee.com/pangxianfei/frame/facades"
	"gitee.com/pangxianfei/frame/kernel/zone"

	"gitee.com/pangxianfei/frame/request"
)

func RequestLogger() request.HandlerFunc {
	return func(c request.Context) {

		if config.GetBool("app.log_out") {
			startedAt := zone.Now()

			// collect request data
			requestHeader := c.Request().Header
			requestData, err := c.GetRawData()
			if err != nil {
				fmt.Println(err.Error())
				c.Next()
			}
			r := c.Request()
			r.Body = ioutil.NopCloser(bytes.NewBuffer(requestData)) // key point
			c.SetRequest(r)

			// collect response data
			responseWriter := &responseWriter{body: bytes.NewBufferString(""), ResponseWriter: c.Writer()}
			c.SetWriter(responseWriter)
			printRequest := c.ClientIP() + " "
			printRequest = printRequest + c.Request().Method + " "
			printRequest = printRequest + c.Request().Method + " "
			printRequest = printRequest + c.Request().RequestURI + " "
			printRequest = printRequest + c.Request().Proto + " "
			printRequest = printRequest + c.Request().Method + " "
			printRequest = printRequest + fmt.Sprintf("%d", responseWriter.Status()) + " "
			printRequest = printRequest + fmt.Sprintf("%s", c.Request().UserAgent()) + " "
			printRequest = printRequest + fmt.Sprintf("%s", zone.Now().Sub(startedAt)) + " "
			printRequest = printRequest + fmt.Sprintf("%s", requestHeader) + " "
			printRequest = printRequest + fmt.Sprintf("%s", string(requestData)) + " "
			printRequest = printRequest + fmt.Sprintf("%s", responseWriter.Header()) + " "
			printRequest = printRequest + fmt.Sprintf("%s", responseWriter.body.String()) + " "
			defer facades.Zap.Info(printRequest)
		}

		c.Next()

		// after request

		// access the status we are sending
	}
}

type responseWriter struct {
	gin.ResponseWriter
	body *bytes.Buffer
}

func (w responseWriter) Write(b []byte) (int, error) {
	w.body.Write(b)
	return w.ResponseWriter.Write(b)
}

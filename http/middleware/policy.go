package middleware

import (
	"gitee.com/pangxianfei/frame/policy"

	"gitee.com/pangxianfei/frame/request"
)

func Policy(_policy policy.Policier, action policy.Action) request.HandlerFunc {
	return func(c request.Context) {
		policy.Middleware(_policy, action, c, c.Params())
	}
}

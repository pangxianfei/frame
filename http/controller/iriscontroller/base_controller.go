package controller

import (
	"github.com/kataras/iris/v12"

	"gitee.com/pangxianfei/frame/filesystem"
	"gitee.com/pangxianfei/frame/http/controller"
	"gitee.com/pangxianfei/frame/irisvalidator"
	"gitee.com/pangxianfei/frame/response"

	contractsfilesystem "gitee.com/pangxianfei/frame/contracts/filesystem"
)

type BaseController struct {
	Context iris.Context
	irisvalidator.Validation
	Request
}
type Request interface {
	File(name string) (filesystem.File, error)
}

func (this *BaseController) File(name string) (contractsfilesystem.File, error) {
	_, file, err := this.Context.FormFile(name)
	if err != nil {
		return nil, err
	}
	return filesystem.NewFileFromRequest(file)
}

func (this *BaseController) Json(data interface{}) *response.JsonResult {
	this.Context.StatusCode(iris.StatusOK)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusOK, "请求成功", data))
	return nil
}

func (this *BaseController) Success(data interface{}) *response.JsonResult {
	this.Context.StatusCode(iris.StatusOK)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusOK, "请求成功", data))
	return nil
}

func (this *BaseController) Fail(data interface{}) *response.JsonResult {
	this.Context.StatusCode(iris.StatusCreated)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusCreated, "请求失败", data))
	return nil
}

func (this *BaseController) Error(data interface{}) *response.JsonResult {
	this.Context.StatusCode(iris.StatusCreated)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusCreated, "请求失败", data))
	return nil
}

func (this *BaseController) JsonData(data interface{}) *response.JsonResult {
	this.Context.StatusCode(iris.StatusOK)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusOK, "查询成功", data))
	return nil
}

// JsonCreateSucces 创建成功返回专用
func (this *BaseController) JsonCreateSucces(data interface{}) *response.JsonResult {
	this.Context.StatusCode(iris.StatusOK)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusOK, "创建成功", data))
	return nil
}

// JsonCreateFail 创建失败返回专用
func (this *BaseController) JsonCreateFail(data interface{}) *response.JsonResult {
	this.Context.StatusCode(iris.StatusCreated)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusCreated, "创建失败,请稍后尝试~", data))
	return nil
}

// JsonQueryData 查询返回专用
func (this *BaseController) JsonQueryData(data interface{}) *response.JsonResult {
	this.Context.StatusCode(iris.StatusOK)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusOK, "查询成功", data))
	return nil
}

// JsonUpdateSuccess 更新成功返回专用
func (this *BaseController) JsonUpdateSuccess(data interface{}) *response.JsonResult {
	this.Context.StatusCode(iris.StatusOK)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusOK, "更新成功", data))
	return nil

}

func (this *BaseController) JsonUpdateFail(data interface{}) *response.JsonResult {
	this.Context.StatusCode(iris.StatusCreated)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusCreated, "更新失败,请稍后尝试~", data))
	return nil
}

func (this *BaseController) JsonDeleteSuccess(data interface{}) *response.JsonResult {
	this.Context.StatusCode(iris.StatusOK)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusOK, "删除成功", data))
	return nil
}

func (this *BaseController) JsonDeleteFail(data string) *response.JsonResult {

	this.Context.StatusCode(iris.StatusCreated)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusCreated, "删除失败,请稍后尝试~", data))
	return nil

}

func (this *BaseController) JsonItemList(data []interface{}) *response.JsonResult {
	this.Context.StatusCode(iris.StatusOK)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusOK, "请求成功", data))
	return nil
}

func (this *BaseController) JsonSuccess() *response.JsonResult {
	this.Context.StatusCode(iris.StatusOK)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusOK, "请求成功", ""))
	return nil
}

func (this *BaseController) JsonErrorMsg(message string) *response.JsonResult {
	this.Context.StatusCode(iris.StatusCreated)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusCreated, "请求失败,请稍后尝试~", message))
	return nil
}

func (this *BaseController) JsonError(err error) *response.JsonResult {
	this.Context.StatusCode(iris.StatusCreated)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusCreated, "请求失败,请稍后尝试~", err.Error()))
	return nil
}

func (this *BaseController) JsonFail(data interface{}) *response.JsonResult {
	this.Context.StatusCode(iris.StatusCreated)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusCreated, "请求失败,请稍后尝试~", data))
	return nil
}

func (this *BaseController) JsonErrorCode(message string) *response.JsonResult {
	this.Context.StatusCode(iris.StatusCreated)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusCreated, "请求失败,请稍后尝试~", message))
	return nil
}

func (this *BaseController) JsonErrorData(message string, data interface{}) *response.JsonResult {
	this.Context.StatusCode(iris.StatusCreated)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusCreated, message, data))
	return nil
}

func (this *BaseController) JsonDataError(data interface{}) *response.JsonResult {
	this.Context.StatusCode(iris.StatusCreated)
	_ = this.Context.JSON(controller.ReturnJson(iris.StatusCreated, "请求失败,请稍后尝试~", data))
	return nil
}

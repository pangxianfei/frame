package controller

import (
	"gitee.com/pangxianfei/frame/response"
	"github.com/kataras/iris/v12"
)

func ReturnJson(httpCode int, message string, data interface{}) *response.JsonResult {
	return &response.JsonResult{
		Code:    httpCode,
		Message: message,
		Data:    data,
		Success: httpCode == iris.StatusOK,
	}
}

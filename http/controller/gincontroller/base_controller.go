package Controller

import (
	"gitee.com/pangxianfei/frame/policy"
	"gitee.com/pangxianfei/frame/request/http/auth"
	"gitee.com/pangxianfei/frame/validator"
)

type BaseController struct {
	policy.Authorization
	auth.RequestUser
	validator.Validation
}

package Controller

import (
	"gitee.com/pangxianfei/frame/policy"
	"gitee.com/pangxianfei/frame/request/http/auth"
	"gitee.com/pangxianfei/frame/validator"
)

type BaseGinController struct {
	validator.Validation
	policy.Authorization
	auth.RequestUser
}

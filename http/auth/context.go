package auth

type Context interface {
	AuthClaimID() (ID uint, exist bool)
	UserModel() IUser
}

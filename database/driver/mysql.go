package driver

import (
	"fmt"
	"gitee.com/pangxianfei/frame/kernel/zone"
	"net/url"
)

type mysql struct {
	ConnDns     string
	DbDriver    string
	Host        string
	UserName    string
	Password    string
	Database    string
	Port        int
	TablePrefix string
	Collation   string
	Charset     string
}

func NewMysql(DriverName string, Host string, Dbuser string, Password string, Database string, Port int, Prefix string, Charset string) *mysql {
	_db := new(mysql)
	_db.setConnection(DriverName, Host, Dbuser, Password, Database, Port, Prefix, Charset)
	return _db
}

func (msl *mysql) setConnection(DriverName string, Host string, UserName string, Password string, Database string, Port int, Prefix string, Charset string) {
	msl.DbDriver = DriverName
	msl.Host = Host
	msl.UserName = UserName
	msl.Password = Password
	msl.Database = Database
	msl.Port = Port
	msl.TablePrefix = Prefix
	msl.Charset = Charset
}

func (msl *mysql) connection() string {
	return msl.ConnDns
}

func (msl *mysql) username() string {
	return msl.UserName
}
func (msl *mysql) password() string {
	return msl.Password
}
func (msl *mysql) host() string {
	return msl.Host
}
func (msl *mysql) port() string {
	return fmt.Sprintf("%d", msl.Port)
}

func (msl *mysql) database() string {
	return msl.Database
}
func (msl *mysql) charset() string {
	return msl.Charset
}
func (msl *mysql) Prefix() string {
	return msl.TablePrefix
}
func (msl *mysql) Driver() string {
	return msl.DbDriver
}
func (msl *mysql) collation() string {
	return msl.ConnectionArgs()
}
func (msl *mysql) config(key string) string {
	return msl.ConnectionArgs()
}

func (msl *mysql) ConnectionArgs() string {
	loc := url.Values{"loc": []string{zone.GetLocation().String()}}
	msl.ConnDns = msl.username() + ":" + msl.password() + "@" + "tcp(" + msl.host() + ":" + msl.port() + ")/" + msl.database() + "?charset=" + msl.charset() + "&parseTime=True&" + loc.Encode()
	return msl.ConnDns
}

package migration

import (
	"fmt"

	"gorm.io/gorm"

	"gitee.com/pangxianfei/frame/library/config"

	"gitee.com/pangxianfei/frame/facades"
)

type Migration struct {
	ID        int64  `gorm:"column:id;primary_key;auto_increment;"`
	Migration string `gorm:"column:migration;type:varchar(255)"`
	Batch     int64  `gorm:"column:batch;"`
}

// 建立migration表
func (m *Migration) up(db *gorm.DB) {
	tx := db.Begin()
	{
		_ = tx.Migrator().CreateTable(&m)
	}
	tx.Commit()
}

func (m *Migration) TableName() string {
	tableName := config.GetString("database.migrations")
	if tableName == "" {
		facades.Log.Info("migrations table name parse error")
		return ""
	}

	tableNameWithPrefix := fmt.Sprintf("%s", tableName)
	return tableNameWithPrefix
}

func (m *Migration) Name() string {
	return m.Migration
}

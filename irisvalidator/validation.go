package irisvalidator

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"gitee.com/pangxianfei/frame/simple"
	zhongwen "github.com/go-playground/locales/zh"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	zhtranslations "github.com/go-playground/validator/v10/translations/zh"
	"github.com/kataras/iris/v12"

	"gitee.com/pangxianfei/frame/helpers"
)

var Validate *validator.Validate
var trans ut.Translator

type ValidationError struct {
	ActualTag string `json:"tag"`
	Namespace string `json:"namespace"`
	Kind      string `json:"kind"`
	Type      string `json:"type"`
	Value     string `json:"value"`
	Param     string `json:"param"`
}

type Validation struct{}

func (v *Validation) Json(ctx iris.Context, requestDataPtr interface{}, Messages map[string][]string) (error, interface{}) {
	var returnData = make(map[string]string)
	var returnMsg string
	if err := ctx.ReadJSON(requestDataPtr); err != nil {
		//异常拦截友好返回错误信息
		if helpers.Empty(Messages) {
			newErr := v.nativeVerification(requestDataPtr)
			for _, MessageBody := range newErr {
				newMsg := v.CustomMessage(Messages, MessageBody.Field(), MessageBody.ActualTag(), MessageBody.Param())
				if returnMsg == "" {
					returnMsg = newMsg
				}
				returnData[MessageBody.Field()] = MessageBody.Translate(trans)
			}
			if len(returnData) > 0 {
				return errors.New(returnMsg), returnData
			}
		} else { //用户定义错误提示
			errs, _ := err.(validator.ValidationErrors)
			validationErrors := v.WrapValidationErrors(errs)
			for _, MessageBody := range validationErrors {
				newMsg := v.CustomMessage(Messages, MessageBody.Namespace, MessageBody.ActualTag, MessageBody.Param)
				if returnMsg == "" {
					returnMsg = newMsg
				}
				returnData[MessageBody.Namespace] = newMsg
			}
			if len(returnData) > 0 {
				return errors.New(returnMsg), returnData
			}
		}
		return err, returnData
	}
	return nil, nil
}

func (v *Validation) nativeVerification(requestDataPtr interface{}) validator.ValidationErrors {

	zh := zhongwen.New()
	uni := ut.New(zh, zh)
	trans, _ = uni.GetTranslator("zh")

	Validate = validator.New()
	Validate.RegisterTagNameFunc(func(field reflect.StructField) string {
		label := field.Tag.Get("label")
		if label == "" {
			return field.Name
		}
		return label
	})
	_ = zhtranslations.RegisterDefaultTranslations(Validate, trans)

	err := Validate.Struct(requestDataPtr)

	if err != nil {
		errs, _ := err.(validator.ValidationErrors)
		return errs
	}
	return nil
}

func (v *Validation) translate(errs validator.ValidationErrors) string {
	var errList []string
	for _, e := range errs {
		errList = append(errList, e.Translate(trans))
	}
	return strings.Join(errList, "|")
}

func (v *Validation) CustomMessage(Messages map[string][]string, field string, label string, Param string) string {
	if len(Messages) <= 0 {
		return ""
	}
	if msgList, ok := Messages[field]; ok {
		for _, m := range msgList {
			if strings.Contains(label, ":") {
				label = strings.Split(label, ":")[0]
			}
			if strings.HasPrefix(m, label+":") {
				msg := strings.TrimPrefix(m, label+":")
				if strings.Contains(msg, label+":") {
					msg = strings.Replace(msg, label+":", Param, -1)
				}
				if strings.Contains(msg, "%s") {
					msg = strings.Replace(msg, "%s", Param, -1)
				}
				return msg
			}
		}
	}
	return ""
}

func (v *Validation) WrapValidationErrors(errs validator.ValidationErrors) []ValidationError {
	validationErrors := make([]ValidationError, 0, len(errs))
	for _, validationErr := range errs {
		Namespace := simple.Split(validationErr.Namespace(), ".")
		validationErrors = append(validationErrors, ValidationError{
			ActualTag: validationErr.ActualTag(),
			Namespace: Namespace,
			Kind:      validationErr.Kind().String(),
			Type:      validationErr.Type().String(),
			Value:     fmt.Sprintf("%v", validationErr.Value()),
			Param:     validationErr.Param(),
		})
	}
	return validationErrors
}
